import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Created by slowinger on 12/15/15.
 */
public class BitfinexMessage
{

    private String[] msg;
    private MessageType type;
    private int channel;
    private double price;
    private int count;
    private double quantity;

    public BitfinexMessage(String message)
    {
        msg = message.substring(1, message.length() - 1).split(",");

        if (msg.length == 4)
        {
            this.type = MessageType.UPDATE;
            this.channel = Integer.parseInt(msg[0]);
            this.price = Double.parseDouble(msg[1]);
            this.count = Integer.parseInt(msg[2]);
            this.quantity = Double.parseDouble(msg[3]);
        } else if (msg.length < 4)
        {
            if (!isLogin())
            {
                this.type = MessageType.HEARTBEAT;
            }
            else { this.type = MessageType.LOGIN; }
        }
        else
        {
            if (isLogin())
            {
                this.type = MessageType.LOGIN;
            }
            else { this.type = MessageType.SNAPSHOT; }
        }
    }

    private boolean isLogin()
    {
        String pattern = "\"event\".*?";
        if (Pattern.matches(pattern, msg[0]))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public String[] getMsg() {return this.msg;}

    public MessageType getType() {return this.type;}

    public int getChannel() {return channel;}

    public double getPrice() {return price;}

    public int getCount() {return count;}

    public double getQuantity() {return quantity;}
}
