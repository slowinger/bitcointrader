import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;

/**
 * Created by slowinger on 11/8/15.
 */
public class CoinbaseMessage implements Comparable<CoinbaseMessage>, Serializable
{
    private String type;
    private Long sequence;
    private String order_id;
    private double size;
    private double price;
    private String side;
    private double remaining_size;
    private String reason;
    private String maker_order_id;
    private String taker_order_id;
    private String time;


    public static CoinbaseMessage parseMessage(String strMessage)
    {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(strMessage, CoinbaseMessage.class);
    }


    @Override
    public String toString()
    {
        return "Message{" +
                "type='" + type + '\'' +
                ", sequence=" + sequence +
                ", order_id='" + order_id + '\'' +
                ", size=" + size +
                ", price=" + price +
                ", side='" + side + '\'' +
                ", remaining_size=" + remaining_size +
                ", reason='" + reason + '\'' +
                ", maker_order_id='" + maker_order_id + '\'' +
                ", taker_order_id='" + taker_order_id + '\'' +
                ", time='" + time + '\'' +
                '}';
    }

    public double getRemaining_size() {
        return remaining_size;
    }

    public void setRemaining_size(double remaining_size) {
        this.remaining_size = remaining_size;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSize() {
        return size;
    }


    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public Long getSequence() {
        return sequence;
    }

    public void setSequence(Long sequence) {
        this.sequence = sequence;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMaker_order_id() {
        return maker_order_id;
    }

    public void setMaker_order_id(String maker_order_id) {
        this.maker_order_id = maker_order_id;
    }

    public String getTaker_order_id() {
        return taker_order_id;
    }

    public void setTaker_order_id(String taker_order_id) {
        this.taker_order_id = taker_order_id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int compareTo(CoinbaseMessage other)
    {
        return Double.compare(this.price, other.getPrice());
    }

    public byte[] messageToBytes()
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;

        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(this);
            return bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        } finally
        {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                // ignore close exception
            }
            try {
                bos.close();
            } catch (IOException ex) {
                // ignore close exception
            }
        }
        return null;

    }
}
