import java.util.*;

/**
 * Created by slowinger on 11/13/15.
 */
public class CoinbaseOrderBook implements OrderBook
{
    private TreeMap<String, CoinbaseMessage> bids;
    private TreeMap<String, CoinbaseMessage> offers;

    public CoinbaseOrderBook()
    {
        bids = new TreeMap();
        offers = new TreeMap();
    }

    public void parseMessage(CoinbaseMessage message)
    {
        String type = message.getType();
        String side = message.getSide();
        String orderId = message.getOrder_id();

        if(type.equals("received"))
        {
            if (side.equals("buy"))
            {
                bids.put(orderId, message);
            } else
            {
                offers.put(orderId, message);
            }
        } else if (type.equals("done"))
        {
            if (side.equals("buy"))
            {
                if (bids.containsKey(orderId))
                {
                    bids.remove(orderId);
                }

            } else
            {
                if (offers.containsKey(orderId))
                {
                    offers.remove(orderId);
                }
            }
        } else if (type.equals("open"))
        {
            double size = message.getRemaining_size();
            if(side.equals("buy"))
            {
                CoinbaseMessage bid =  bids.get(orderId);
                if (bid != null)
                {
                    bid.setRemaining_size(size);
                }
            } else
            {
                CoinbaseMessage bid =  offers.get(orderId);
                if (bid != null)
                {
                    bid.setRemaining_size(size);
                }
            }
        }
    }

    @Override
    public double getBestBid()
    {
        if (!bids.isEmpty()) {return Collections.max(bids.values()).getPrice();}
        else {return 0;}
    }

    @Override
    public double getBestOffer()
    {
        if (!offers.isEmpty()) {return Collections.min(offers.values()).getPrice();}
        else {return 0;}
    }

    private double getSize(TreeMap<String, CoinbaseMessage> orders, double price)
    {
        if (!orders.isEmpty())
        {
            return orders.values().stream()
                    .filter(o -> o.getPrice() == price)
                    .mapToDouble(CoinbaseMessage::getSize)
                    .sum();
        } else {return 0;}
    }

    @Override
    public double getBestBidSize()
    {
        return getSize(bids, getBestBid());
    }

    @Override
    public double getBestOfferSize()
    {
        return getSize(offers, getBestOffer());
    }
}
