/**
 * Created by slowinger on 11/8/15.
 */

import java.io.*;
import java.net.URI;
import java.util.concurrent.Future;

import org.apache.commons.io.IOUtils;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


@WebSocket
public class ExchangeListener
{
    private static final String CONFIG_FILE = "/Users/slowinger/Desktop/Bitcoin/BitcoinClient/config/ExchangeListener/Config.xml";

    private static String host;

    private static String queueName;

    private static String url;

    private static String subscribeMessagePath;

    private static String subscribeMessage;

    private static final Logger LOG = Log.getLogger(ExchangeListener.class);

    private static Publisher publisher;


    public static void main(String[] args) throws FileNotFoundException
    {
        loadConfig();

        SslContextFactory sslContextFactory = new SslContextFactory();
        sslContextFactory.setTrustAll(true); // The magic

        WebSocketClient client = new WebSocketClient(sslContextFactory);

        publisher = new Publisher(host);
        publisher.run();

        try
        {
            client.start();
            ExchangeListener socket = new ExchangeListener();
            Future<Session> fut = client.connect(socket, URI.create(url));
            Session session = fut.get();
            session.getRemote().sendString(subscribeMessage);
            //session.getRemote().sendString("155-questions-active");

        } catch (Throwable t)
        {
            LOG.warn(t);
        }
    }

    @OnWebSocketConnect
    public void onConnect(Session sess)
    {
        LOG.info("onConnect({})", sess);
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason)
    {
        LOG.info("onClose({}, {})", statusCode, reason);
    }

    @OnWebSocketError
    public void onError(Throwable cause)
    {
        LOG.warn(cause);

        try
        {
            publisher.getChannel().close();
            publisher.getConnection().close();
            LOG.warn("Shutting Down");
            System.exit(0);
        } catch (Exception e)
        {
            LOG.warn(e.getStackTrace().toString());
            System.exit(0);

        }

    }

    @OnWebSocketMessage
    public void onMessage(String strMessage)
    {
        if (queueName.equals("Coinbase"))
        {
            CoinbaseMessage msg = CoinbaseMessage.parseMessage(strMessage);

            try
            {
                publisher.getChannel().basicPublish("", queueName, null, msg.messageToBytes());
                LOG.info("Published: {}", strMessage);
            } catch (IOException e)
            {
                LOG.warn(e.getStackTrace().toString());
                LOG.warn("Shutting Down");
                System.exit(0);
            }
        } else
        {
            try {
                publisher.getChannel().basicPublish("", queueName, null, strMessage.getBytes());
                LOG.info("Published: {}", strMessage);
            } catch (IOException e) {
                LOG.warn(e.getStackTrace().toString());
                LOG.warn("Shutting Down");
                System.exit(0);
            }

        }

    }

    private static void loadConfig() throws FileNotFoundException
    {
        final String configFile = CONFIG_FILE;

        LOG.info("configFile: " + configFile);


        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document config = builder.parse(new File(configFile));
            Element rootElement = config.getDocumentElement();

            NodeList nodes = rootElement.getChildNodes();

            Node node, childNode;
            for (int i = 0; i < nodes.getLength(); i++)
            {
                node = nodes.item(i);
                if (node.getNodeType()==Node.ELEMENT_NODE)
                {
//                    System.out.println(((Element) node).getTagName());
                    switch (((Element) node).getTagName())
                    {
                        case "host":
                            host = node.getFirstChild().getNodeValue();
                            LOG.info("Host set to: " + host);
                            break;
                        case "queue-name":
                            queueName = node.getFirstChild().getNodeValue();
                            LOG.info("Queue name set to: " + queueName);
                            break;
                        case "url":
                            url = node.getFirstChild().getNodeValue();
                            LOG.info("url set to: " + url);
                            break;
                        case "subscribe":
                            subscribeMessagePath = node.getFirstChild().getNodeValue();
                            LOG.info("subscribe message path set to: " + subscribeMessagePath);
                            break;
                    }
                }
            }

        } catch (Exception e)
        {
            LOG.warn(e.getStackTrace().toString());
            System.exit(0);
        }

        loadSubscribeMessage();
        LOG.info(subscribeMessage);
    }

    private static void loadSubscribeMessage()
    {
        File subscribeJson = new File(subscribeMessagePath);
        InputStream subscribeStream = null;
        try {
            subscribeStream = new FileInputStream(subscribeJson);
            subscribeMessage = IOUtils.toString( subscribeStream );
        } catch (Exception e) {
            LOG.warn(e.getStackTrace().toString());
            System.exit(0);

        }
        if (subscribeMessage==null)
        {
            LOG.warn("Shutting down, Subscribe message is null");
            System.exit(0);
        }
    }
}
