//import com.xeiam.xchange.Exchange;
//import com.xeiam.xchange.ExchangeFactory;
//import com.xeiam.xchange.bitstamp.BitstampExchange;
//import com.xeiam.xchange.bitstamp.service.streaming.BitstampStreamingConfiguration;
//import com.xeiam.xchange.dto.marketdata.OrderBook;
//import com.xeiam.xchange.dto.marketdata.Trade;
//import com.xeiam.xchange.service.streaming.ExchangeEvent;
//import com.xeiam.xchange.service.streaming.StreamingExchangeService;
//import org.eclipse.jetty.util.log.Log;
//import org.eclipse.jetty.util.log.Logger;
//import org.eclipse.jetty.websocket.api.annotations.WebSocket;
//
//import java.io.IOException;
//
///**
// * Created by slowinger on 11/9/15.
// */
//@WebSocket
//public class ExchangeListener2
//{
//    private static final Logger LOG = Log.getLogger(ExchangeListener2.class);
//
//    public static void main(String[] args) throws Exception {
//
//        // Use the factory to get Bitstamp exchange API using default settings
//        Exchange bitstamp = ExchangeFactory.INSTANCE.createExchange(BitstampExchange.class.getName());
//        BitstampStreamingConfiguration streamCfg = new BitstampStreamingConfiguration();
//        // Interested in the public streaming market data feed (no authentication)
//        StreamingExchangeService streamService = bitstamp.getStreamingExchangeService(streamCfg);
//        streamService.connect();
//        go(streamService);
////        streamService.disconnect();
//    }
//
//    private static void go(StreamingExchangeService streamService) throws IOException
//    {
//        boolean exit = false;
//
//        try {
//            while(!exit)
//             {
//                ExchangeEvent evt = streamService.getNextEvent();
//                switch (evt.getEventType()) {
//                    case SUBSCRIBE_ORDERS:
//                        LOG.info(evt.getPayload().toString());
//                        //System.out.println(((OrderBook) evt.getPayload()).toString());
//                        break;
//                    case DEPTH:
//                        LOG.info(evt.getPayload().toString());
//                        //System.out.println(((OrderBook) evt.getPayload()).toString());
//                        break;
//                    case TRADE:
//                        LOG.info("Trade: ", evt.getPayload().toString());
//                        //System.out.println(((Trade) evt.getPayload()).toString());
//                        break;
//                    default:
//                        exit = true;
//                        break;
//                }
//
//            }
//            System.out.println("Closing Bitstamp stream.");
//        } catch (InterruptedException e) {
//            e.printStackTrace(System.err);
//        }
//    }
//}

