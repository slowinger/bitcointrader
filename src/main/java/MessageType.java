/**
 * Created by slowinger on 12/20/15.
 */
public enum MessageType {UPDATE, LOGIN, SNAPSHOT, HEARTBEAT}
