/**
 * Created by slowinger on 12/20/15.
 */
public interface OrderBook
{
    double getBestBid();

    double getBestOffer();

    double getBestBidSize();

    double getBestOfferSize();
}
