import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import sun.jvm.hotspot.HotSpotTypeDataBase;

/**
 * Created by slowinger on 11/9/15.
 */
public class Publisher implements Runnable
{
    private final String HOST;

    private Connection connection;
    private ConnectionFactory factory;
    private Channel channel;


    public Publisher(String strHost)
    {
        this.factory = new ConnectionFactory();;
        this.HOST = strHost;
    }

    public void run()
    {
        factory.setHost(HOST);
        channel = null;
        try
        {
            connection = factory.newConnection();
            channel = connection.createChannel();

        } catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public Connection getConnection() {return connection;}

    public Channel getChannel() {return channel;}
}
