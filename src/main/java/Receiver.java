import com.rabbitmq.client.*;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Receiver
{
    private final static String COINBASE_QUE = "Coinbase";
    private final static String BITFINEX_QUE = "bitfinex";

    private static final Logger LOG = Log.getLogger(Receiver.class);

    private static Map<String, Object> args = new HashMap<String, Object>();

    private static CoinbaseOrderBook orderBook;

    public static void main(String[] argv) throws Exception {
        orderBook = new CoinbaseOrderBook();

        args.put("x-message-ttl", 5000);

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel coinbaseChannel = connection.createChannel();



        coinbaseChannel.queueDeclare(COINBASE_QUE, false, false, false, args);
        LOG.info(" Starting receiver.  Waiting for messages.");

        Consumer consumer = new DefaultConsumer(coinbaseChannel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
//                String message = new String(body, "UTF-8");
                //CoinbaseMessage msg = CoinbaseMessage.parseMessage(message);

                ByteArrayInputStream bis = new ByteArrayInputStream(body);
                ObjectInput in = null;
                try {
                    in = new ObjectInputStream(bis);
                    Object msg = in.readObject();

                    if (msg instanceof CoinbaseMessage)
                    {
                        CoinbaseMessage cbMessage = (CoinbaseMessage) msg;
                        orderBook.parseMessage(cbMessage);
                        LOG.info(COINBASE_QUE + " Message: " +  msg);
                        LOG.info(
                                " Orderbook {}", "Price: " +  orderBook.getBestBid() + " @ " + orderBook.getBestOffer() +
                                        " Size: " + orderBook.getBestBidSize() + " @ " + orderBook.getBestOfferSize());
                    }
                } catch (ClassNotFoundException e)
                {
                  LOG.warn(e.getStackTrace().toString());
                } finally
                {
                    try {
                        bis.close();
                    } catch (IOException ex) {
                        // ignore close exception
                    }
                    try {
                        if (in != null) {
                            in.close();
                        }
                    } catch (IOException ex) {
                        // ignore close exception
                    }
                }


            }
        };
        coinbaseChannel.basicConsume(COINBASE_QUE, true, consumer);

        Channel bitfinexChannel = connection.createChannel();

        bitfinexChannel.queueDeclare(BITFINEX_QUE, false, false, false, args);


        Consumer bitfinexConsumer = new DefaultConsumer(bitfinexChannel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String message = new String(body, "UTF-8");
                BitfinexMessage bMessage = new BitfinexMessage(message);
                LOG.info(BITFINEX_QUE + " " + bMessage.getType() + ": " +  Arrays.toString(bMessage.getMsg()));
            }
        };
        bitfinexChannel.basicConsume(BITFINEX_QUE, true, bitfinexConsumer);
    }
}



